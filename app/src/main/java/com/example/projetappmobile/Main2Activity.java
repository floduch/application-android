package com.example.projetappmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;


public class Main2Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final Spinner spinnerTemps = findViewById(R.id.spinner);
        final Spinner spinnerPerson = findViewById(R.id.spinner1);
        final Spinner spinnerDiffi = findViewById(R.id.spinner2);
        final Spinner spinnerCouts = findViewById(R.id.spinner3);

        spinnerTemps.setOnItemSelectedListener(this);
        spinnerPerson.setOnItemSelectedListener(this);
        spinnerDiffi.setOnItemSelectedListener(this);
        spinnerCouts.setOnItemSelectedListener(this);

        Button submit = findViewById(R.id.boutonSubmit);
        ImageButton retour = findViewById(R.id.imageButton);

        final Intent intent = new Intent(this, MainActivity.class);
        final Intent intent1 = new Intent(this, Main3Activity.class);

//        Temps
        ArrayAdapter<CharSequence> adapterTemps= ArrayAdapter.createFromResource(this, R.array.temps, android.R.layout.simple_spinner_item);
        spinnerTemps.setPrompt("Temps Disponible");
        adapterTemps.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTemps.setAdapter(adapterTemps);

//        Personnes
        ArrayAdapter<CharSequence> adapterPers = ArrayAdapter.createFromResource(this, R.array.personne, android.R.layout.simple_spinner_item);
        spinnerPerson.setPrompt("Nombres de Personnes");
        adapterPers.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerPerson.setAdapter(adapterPers);

//        Difficulté
        ArrayAdapter<CharSequence> adapterDiff= ArrayAdapter.createFromResource(this, R.array.difficulte, android.R.layout.simple_spinner_item);
        spinnerDiffi.setPrompt("Difficulté");
        adapterDiff.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerDiffi.setAdapter(adapterDiff);

//        Coûts
        ArrayAdapter<CharSequence> adapterCouts = ArrayAdapter.createFromResource(this, R.array.couts, android.R.layout.simple_spinner_item);
        spinnerCouts.setPrompt("Coûts");
        adapterCouts.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerCouts.setAdapter(adapterCouts);


        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intent);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intent1.putExtra("Temps", String.valueOf(spinnerTemps.getSelectedItem()));
                intent1.putExtra("Personne", String.valueOf(spinnerPerson.getSelectedItem()));
                intent1.putExtra("Difficulte", String.valueOf(spinnerDiffi.getSelectedItem()));
                intent1.putExtra("Couts", String.valueOf(spinnerCouts.getSelectedItem()));
                startActivity(intent1);
            }
        });
    }

     public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
         String item = parent.getItemAtPosition(pos).toString();
     }

     public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
}