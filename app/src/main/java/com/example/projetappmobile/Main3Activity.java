package com.example.projetappmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final Intent intent = new Intent(this, Main2Activity.class);
        final Intent intent1 = getIntent();
        String temps = intent1.getStringExtra("Temps");
        String perso = intent1.getStringExtra("Personne");
        String diffi = intent1.getStringExtra("Difficulte");
        String couts = intent1.getStringExtra("Couts");

        final TextView tempsTexte = (TextView) findViewById(R.id.textView4);
        final TextView tempsPerso = (TextView) findViewById(R.id.textView5);
        final TextView tempsDiffi = (TextView) findViewById(R.id.textView6);
        final TextView tempsCouts = (TextView) findViewById(R.id.textView7);

        tempsTexte.setText(temps);
        tempsPerso.setText(perso);
        tempsDiffi.setText(diffi);
        tempsCouts.setText(couts);


        ImageButton retour = findViewById(R.id.imageButton);
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intent);
            }
        });


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
